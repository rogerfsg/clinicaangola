﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClinicaWebApplication.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClinicaWebApplication.Models;
using ClinicaWebApplication.Services;
using ClinicaWebApplication.Util;
using NSubstitute;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.Security.Principal;

namespace ClinicaWebApplication.Controllers.Tests
{
    [TestClass()]
    public class AgendaControllerTests
    {
        [TestMethod()]
        public void CalendarioTest()
        {
            var servicos = new List<ServicoModel>
            {
                new ServicoModel{
                    ID = 1
                    , Nome= "massagem1"
                    , DuracaoMinutos = 60
                    , Intervalo = 2
                    , TipoParaCancelamento = ServicoModel.TipoCancelamento.DIA_CORRIDO
                    , UnidadeMaximaParaCancelamento = 24 },
            };

            var agendamentos = new List<AgendaModel>();

            IUsuario iUsuario = Substitute.For<IUsuario>();
            iUsuario.GetUserId().Returns("1");
            
            IFactoryProvider provider = Substitute.For<IFactoryProvider>();
            

            provider.FactoryServicoDataProvider(null).ReturnsForAnyArgs(new ServicoMock(servicos));
            provider.FactoryAgendaDataProvider(null,null).ReturnsForAnyArgs(new AgendaMock(null,agendamentos));
            provider.FactoryUsuarioDataProvider(null, null).ReturnsForAnyArgs(new UsuarioMock(new ApplicationUser { Nome = "Roger" }));
            provider.FactoryProvider().ReturnsForAnyArgs(new ProviderBancoDeDados());
            AgendaController agendaController = new AgendaController();
            agendaController.Usuario = iUsuario;

            FactoryContext context = FactoryContext.GetSingleton();
            context.FactoryDataProvider = provider;

            Task<ActionResult> x= agendaController.Calendario(1, null, null, null, null);
            //x.Start();
            x.Wait();
            

            ActionResult a= x.Result;
            Assert.IsInstanceOfType(a, typeof(ViewResult));
            
        }
    }
}