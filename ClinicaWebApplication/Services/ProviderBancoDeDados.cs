﻿using ClinicaWebApplication.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ClinicaWebApplication.Services
{
    public class ProviderBancoDeDados : IProvider
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public ProviderBancoDeDados()
        {   
        }

        public void Dispose()
        {
            if (db != null)
            {
                db.Dispose();
                db = null;
            }
        }

        public object getProvider()
        {
            return db;
        }
    }
}