﻿using ClinicaWebApplication.Models;

using System.Collections.Generic;
using System.Data.Entity;

using System.Threading.Tasks;


namespace ClinicaWebApplication.Services
{
    public class ServicoBancoDeDados : IServicoDataProvider
    {   
        ApplicationDbContext db;
        public ServicoBancoDeDados(IProvider provider)
        {   
            db = (ApplicationDbContext)provider.getProvider() ;
        }

        public ServicoModel Get(int id)
        {
            return db.ServicoModels.Find(id);
        }


        public async Task<List<ServicoModel>> GetServicos()
        {
            return await db.ServicoModels.ToListAsync();
        }

    }
}