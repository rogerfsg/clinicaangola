﻿using ClinicaWebApplication.Model;
using ClinicaWebApplication.Models;

using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;


namespace ClinicaWebApplication.Services
{
    public class AgendaBancoDeDados : IAgendaDataProvider
    {
        
        //IFactoryAgenda factoryAgenda = FactoryContext.GetSingleton().FactoryAgenda;
        //ConfiguracaoAgenda conf = factoryAgenda.factoryConfiguracao();
        ConfiguracaoAgenda conf;
        ApplicationDbContext db;
        public AgendaBancoDeDados(IProvider provider, ConfiguracaoAgenda conf)  
        {   
            this.conf = conf;
            db = (ApplicationDbContext)provider.getProvider();
        }


        public async Task<List<AgendaModel>> GetHorariosMarcados()
        {   
            return await db.AgendaModels
                   .Where(
                    x => 
                        x.DataInicioConsulta >= conf.StartWeek
                        && x.DataFimConsulta <= conf.EndWeek
                    )
                   .OrderBy(x => x.DataInicioConsulta)
                   .ToListAsync();
        }

        public bool MarcarConsulta(AgendaModel agenda)
        {
            db.AgendaModels.Add(agenda);
            return db.SaveChanges() > 0;
        }

        public void CancelarConsulta(int idAgendamento)
        {
            db.AgendaModels.Remove(db.AgendaModels.Find(idAgendamento));
            db.SaveChanges();
        }
    }
}