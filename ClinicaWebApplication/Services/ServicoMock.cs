﻿using ClinicaWebApplication.Models;

using System.Collections.Generic;
using System.Data.Entity;

using System.Threading.Tasks;


namespace ClinicaWebApplication.Services
{
    public class ServicoMock: IServicoDataProvider
    {
        List<ServicoModel> servicos = null;
        public ServicoMock(List<ServicoModel> x = null)
        {
            servicos = x==null?new List<ServicoModel>():x;
        }

        public ServicoModel Get(int id)
        {
            return servicos.Find(x => x.ID == id);
        }


        public Task<List<ServicoModel>> GetServicos()
        {
            var t = new Task<List<ServicoModel>>(() => servicos);
            t.Start();
            return t;
            
        }

    }
}