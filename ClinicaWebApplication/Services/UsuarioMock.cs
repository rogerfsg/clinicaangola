﻿using ClinicaWebApplication.Models;


namespace ClinicaWebApplication.Services
{
    public class UsuarioMock: IUsuarioDataProvider
    {
        ApplicationUser user = null;


        public UsuarioMock(ApplicationUser user=null)
        {
            this.user = user;
        }

        public string GetEmail()
        {
            return user == null ? null : user.Email;
        }

        public string GetId()
        {
            return user == null ? null : user.Id;
        }

        public ApplicationUser getUser()
        {
            return user;
        }
    }
}