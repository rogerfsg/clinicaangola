﻿using ClinicaWebApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ClinicaWebApplication.Services
{
    public interface IServicoDataProvider 
    {
       
        /// <summary>
        /// Retorna o model relativo ao id do servico
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ServicoModel Get(int id);

        /// <summary>
        /// Retorna a lista de servicos
        /// </summary>
        /// <returns></returns>
        Task<List<ServicoModel>> GetServicos();

    }
}
