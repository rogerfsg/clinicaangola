﻿using ClinicaWebApplication.Model;
using ClinicaWebApplication.Models;

using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;


namespace ClinicaWebApplication.Services
{
    public class AgendaMock: IAgendaDataProvider
    {
        
        //IFactoryAgenda factoryAgenda = FactoryContext.GetSingleton().FactoryAgenda;
        //ConfiguracaoAgenda conf = factoryAgenda.factoryConfiguracao();
        ConfiguracaoAgenda conf;
        List<AgendaModel> agendamentos;
        public AgendaMock(ConfiguracaoAgenda conf=null, List<AgendaModel> agendamentos=null)  
        {
            this.conf = conf ==null ? new ConfiguracaoAgenda(): conf;
            this.agendamentos = agendamentos == null ? new List<AgendaModel>() : agendamentos;
        }


        public Task<List<AgendaModel>> GetHorariosMarcados()
        {
            List<AgendaModel> horarios = agendamentos.Where(x =>
                       x.DataInicioConsulta >= conf.StartWeek
                       && x.DataFimConsulta <= conf.EndWeek)
                       .OrderBy(x => x.DataInicioConsulta)
                        .ToList();
            var t =new Task<List<AgendaModel>>(() => horarios);
            t.Start();
            return t;
        }

        public bool MarcarConsulta(AgendaModel agenda)
        {
            agendamentos.Add(agenda);
            return true;
        }

        public void CancelarConsulta(int idAgendamento)
        {
            agendamentos.RemoveAll(x => x.ID == idAgendamento);
        }
    }
}