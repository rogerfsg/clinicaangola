﻿using ClinicaWebApplication.Models;


namespace ClinicaWebApplication.Services
{
    public class UsuarioBancoDeDados : IUsuarioDataProvider
    {

        string userId;
        ApplicationDbContext db;
        public UsuarioBancoDeDados(IProvider provider, string userId)
        {   
            this.userId = userId;
            this.db = (ApplicationDbContext)provider.getProvider() ;
        }

        public string GetEmail()
        {
            var user = db.Users.Find(userId);
            return user == null ? null : user.Email;
        }

        public string GetId()
        {
            return userId;
        }

        public ApplicationUser getUser()
        {
            return db.Users.Find(userId); 
        }
    }
}