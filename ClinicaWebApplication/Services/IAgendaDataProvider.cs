﻿using ClinicaWebApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ClinicaWebApplication.Services
{
    public interface IAgendaDataProvider 
    {
        Task<List<AgendaModel>> GetHorariosMarcados();
        
        bool MarcarConsulta(AgendaModel agenda);
        void CancelarConsulta(int idAgendamento);
    }
}