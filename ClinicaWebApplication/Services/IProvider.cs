﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicaWebApplication.Services
{
    /// <summary>
    /// Provider de dados
    /// </summary>
    public interface IProvider : IDisposable
    {
        object getProvider();
    }
}