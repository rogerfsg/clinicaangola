﻿using ClinicaWebApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ClinicaWebApplication.Services
{
    public interface IUsuarioDataProvider 
    {
       /// <summary>
       /// Retorna o email do usuario logado
       /// </summary>
       /// <returns></returns>
        string GetEmail();
        /// <summary>
        /// Retorna o id do usuario logado
        /// </summary>
        /// <returns></returns>
        string GetId();

        /// <summary>
        /// Retorna o objeto do usuário logado
        /// </summary>
        /// <returns></returns>
        ApplicationUser getUser();
    }
}
