﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClinicaWebApplication.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if(!User.Identity.IsAuthenticated)
            {
                Util.FactoryContext factory = Util.FactoryContext.GetSingleton();
                ViewBag.Title = factory.HelperAppSettings.GetString("Titulo");
                return View();
            } else
            {
                return RedirectToAction("Calendario", "Agenda");
            }
        }

    }
}