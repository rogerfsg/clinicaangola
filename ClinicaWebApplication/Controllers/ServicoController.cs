﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClinicaWebApplication.Models;

namespace ClinicaWebApplication.Controllers
{
    public class ServicoController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Servico
        public ActionResult Index()
        {
            return View(db.ServicoModels.ToList());
        }

        // GET: Servico/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServicoModel servicoModel = db.ServicoModels.Find(id);
            if (servicoModel == null)
            {
                return HttpNotFound();
            }
            return View(servicoModel);
        }

        // GET: Servico/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Servico/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Nome,DuracaoMinutos,Intervalo,TipoParaCancelamento,UnidadeMaximaParaCancelamento")] ServicoModel servicoModel)
        {
            if (ModelState.IsValid)
            {
                db.ServicoModels.Add(servicoModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(servicoModel);
        }

        // GET: Servico/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServicoModel servicoModel = db.ServicoModels.Find(id);
            if (servicoModel == null)
            {
                return HttpNotFound();
            }
            return View(servicoModel);
        }

        // POST: Servico/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Nome,DuracaoMinutos,Intervalo,TipoParaCancelamento,UnidadeMaximaParaCancelamento")] ServicoModel servicoModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(servicoModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(servicoModel);
        }

        // GET: Servico/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServicoModel servicoModel = db.ServicoModels.Find(id);
            if (servicoModel == null)
            {
                return HttpNotFound();
            }
            return View(servicoModel);
        }

        // POST: Servico/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ServicoModel servicoModel = db.ServicoModels.Find(id);
            db.ServicoModels.Remove(servicoModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
