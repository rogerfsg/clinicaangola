﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ClinicaWebApplication.Model;
using ClinicaWebApplication.Models;
using ClinicaWebApplication.Services;
using ClinicaWebApplication.Util;
using Microsoft.AspNet.Identity;

namespace ClinicaWebApplication.Controllers
{
    public class AgendaController : BaseController
    {


        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        /// <summary>
        /// Cancelar a consulta
        /// </summary>
        /// <param name="selecionadoServicoId"></param>
        /// <param name="agendaModel"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult CancelarConsulta(int? selecionadoServicoId, int? idAgenda, DateTime? dataDaSemana)
        {
            FactoryContext factoryContext = FactoryContext.GetSingleton();
            IFactoryAgenda factoryAgenda = factoryContext.FactoryAgenda(GetUserId, dataDaSemana);
            if (idAgenda != null) {
                using (IProvider provider = factoryContext.FactoryProvider())
                {
                    IAgendaDataProvider agendaProvider = factoryContext.FactoryAgendaDataProvider(provider, factoryAgenda);
                    agendaProvider.CancelarConsulta(idAgenda.Value);
                }
            }
                
            
            return RedirectToAction("Calendario", "Agenda", new { selecionadoServicoId = selecionadoServicoId, dataDaSemana  });
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="idSselecionadoServicoIdervico"></param>
        /// <param name="indexEscolha">Consulta particionadas mantes a contagem de quantas ainda faltam</param>
        /// <returns></returns>
        [Authorize]
        // GET: Agenda/MarcarConsulta
        public ActionResult MarcarConsulta(
            DateTime? dataInicio, 
            DateTime? dataFim, 
            int? selecionadoServicoId, 
            int? contadorAgendamento, 
            DateTime? dataDaSemana)
        {   
            if (selecionadoServicoId != null)
            {
                AgendaModel agendaModel = new AgendaModel();
                FactoryContext factoryContext = FactoryContext.GetSingleton();
                IFactoryAgenda factoryAgenda = factoryContext.FactoryAgenda(GetUserId, dataDaSemana);
                using (IProvider provider = factoryContext.FactoryProvider())
                {
                    IUsuarioDataProvider usuarioProvider = factoryContext.FactoryUsuarioDataProvider(provider, GetUserId);
                    IServicoDataProvider servicoProvider = factoryContext.FactoryServicoDataProvider(provider);

                    agendaModel.ServicoId = selecionadoServicoId.Value;
                    agendaModel.DataInicioConsulta = dataInicio;
                    agendaModel.DataFimConsulta = dataFim;
                    agendaModel.UserId = usuarioProvider.GetId();
                    agendaModel.Servico = servicoProvider.Get(selecionadoServicoId.Value);
               
                    IAgendaDataProvider agendaProvider = factoryContext.FactoryAgendaDataProvider(provider, factoryAgenda);
                    if (!agendaProvider.MarcarConsulta(agendaModel))
                    {
                        return RedirectToAction("Calendario", "Agenda",
                            new { selecionadoServicoId, contadorAgendamento, dataDaSemana, mensagemUsuario = "Não foi possível realizar a marcação, tente novamente... Se persistir contacte nos." });
                    }
                }

                //Da andamento na marcacao do proximo intervalo da consulta
                if (agendaModel.Servico.Intervalo > 1)
                {
                    if (contadorAgendamento == null) contadorAgendamento = 1;
                    else if (contadorAgendamento + 1 < agendaModel.Servico.Intervalo) contadorAgendamento += 1;
                    else contadorAgendamento = null;
                }
                else contadorAgendamento = null;
                
            }
            return RedirectToAction("Calendario", "Agenda", 
                new { selecionadoServicoId, contadorAgendamento, dataDaSemana });
        }
        public enum AcaoCalendario
        {
            SEMANA_POSTERIOR,
            SEMANA_ANTERIOR
               
        }
        /// <summary>
        /// As acoes mapeadas ate o momento sao referentes a acao de botao: ir para proxima semana, ir para semana anterior
        /// </summary>
        /// <param name="dataDaSemana">Alguma data da semana que esta sendo exibida</param>
        /// <param name="acaoCalendario">Acao do calendario</param>
        /// <returns></returns>
        private DateTime? executaAcaoCalendario(DateTime? dataDaSemana, AcaoCalendario acaoCalendario)
        {   
            return 
                acaoCalendario == AcaoCalendario.SEMANA_ANTERIOR 
                ? dataDaSemana.Value.AddDays(-7) 
                : dataDaSemana.Value.AddDays(7);
            
        }
        //
        // GET: /Agenda/Calendario
        /// <summary>
        /// 
        /// </summary>
        /// <param name="selecionadoServicoId"></param>
        /// <param name="contadorAgendamento"></param>
        /// <param name="semanaPosterior"></param>
        /// <param name="semanaAnterior">Seta true para a proxima semana</param>
        /// <returns></returns>
        [Authorize]
        public async Task<ActionResult> Calendario(
            int? selecionadoServicoId, 
            int? contadorAgendamento, 
            AcaoCalendario? acaoCalendario,
            DateTime? dataDaSemana,
            string mensagemUsuario)
        {
            FactoryContext factory = FactoryContext.GetSingleton();
            dataDaSemana = dataDaSemana == null ? FactoryContext.GetSingleton().HelperDate.Now() : dataDaSemana;
            if (acaoCalendario != null)
                dataDaSemana = executaAcaoCalendario(dataDaSemana, acaoCalendario.Value);
            IFactoryAgenda factoryAgenda = factory.FactoryAgenda(GetUserId, dataDaSemana);
            
            List<AgendaModel> horariosMarcados = null;
            List<ServicoModel> servicos = null;
            ServicoModel selecionadoServico=null;
            
            SelectList slServicos = null;
            string emailUsuario = null;
            using (IProvider provider = factory.FactoryProvider())
            {
                IAgendaDataProvider dataProvider = factory.FactoryAgendaDataProvider(provider, factoryAgenda);
                IServicoDataProvider servicoDataProvider = factory.FactoryServicoDataProvider(provider);
                horariosMarcados = await dataProvider.GetHorariosMarcados();
                servicos = await servicoDataProvider.GetServicos();
                slServicos = new SelectList(servicos, "ID", "Nome");
                if (selecionadoServicoId != null)
                    selecionadoServico = servicos.First(x=>x.ID== selecionadoServicoId);

                 IUsuarioDataProvider usuarioDP = factory.FactoryUsuarioDataProvider(provider, GetUserId);
                 emailUsuario = usuarioDP.GetEmail();
            }
            //Checando se as condicionais do servico selecionado
            //quem inicializa o indexEscolhar é o MarcarConsulta
            //analisa se já é a ultima escolha para marcação
            if (selecionadoServico != null && contadorAgendamento != null && contadorAgendamento > selecionadoServico.Intervalo)
            { 
                contadorAgendamento = null;
            }

            ConfiguracaoAgenda configuracao = factoryAgenda.factoryConfiguracao();
            CalendarioModel cm = new CalendarioModel
            {
                DiasDaSemana = factoryAgenda.factoryDays(horariosMarcados),
                TituloDiasSemana = configuracao.Days,
                TituloMes = dataDaSemana.Value. ToString("MMM", configuracao.CultureInfo),
                Servicos = slServicos,
                SelecionadoServicoId = selecionadoServicoId,
                ContadorAgendamento = contadorAgendamento,
                SelecionadoServico = selecionadoServico ,
                DataDaSemana= dataDaSemana
            };

            return View(cm);
        }


    }
}
