﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ClinicaWebApplication.Model;
using ClinicaWebApplication.Models;
using ClinicaWebApplication.Services;
using ClinicaWebApplication.Util;
using Microsoft.AspNet.Identity;

namespace ClinicaWebApplication.Controllers
{
  
    public interface IUsuario
    {
        string GetUserId();
    }
    public class Usuario : IUsuario
    {
        string id;
        public Usuario(string id)
        { this.id = id; }
        public string GetUserId() { return id; }
    }
    public abstract class BaseController : Controller
    {

        public IUsuario Usuario { get; set; }
        public BaseController()
        {
                Usuario = User != null && User.Identity != null  ? new Usuario(User.Identity.GetUserId()) : null;
        }
        public string GetUserId
        {
            get
            {
                return Usuario != null 
                    ? Usuario.GetUserId()
                : null;
            }
        }

        public bool IsAuthenticated()
        {
            return Usuario != null;
        }
    }
}
