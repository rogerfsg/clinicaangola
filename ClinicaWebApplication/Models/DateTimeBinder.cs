﻿using ClinicaWebApplication.Util;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Web.Mvc;

namespace ClinicaWebApplication.Models
{
    public class DateTimeBinder : DefaultModelBinder
    {
        CultureInfo CultureInfo { get; set; }
        public DateTimeBinder()
        {
            CultureInfo = FactoryContext.GetSingleton().CultureInfo;
        }
        /// <summary>
        /// Bind para cada property do modelo <see cref="ModelBindingContext"/>
        /// </summary>
        /// <param name="controllerContext">contexto do controller</param>
        /// <param name="bindingContext">contexto do binding</param>
        /// <param name="propertyDescriptor">informacoes da property que esta sofrendo o binding</param>
        protected override void BindProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor)
        {
            Type propertyType = propertyDescriptor.PropertyType;
            if (propertyType != typeof(DateTime?) && propertyType != typeof(DateTime))
            {
                base.BindProperty(controllerContext, bindingContext, propertyDescriptor);
                return;
            }

            ValueProviderResult provider = bindingContext.ValueProvider.GetValue(propertyDescriptor.Name);

            if (provider != null)
            {
                if (propertyType == typeof(DateTime?) && string.IsNullOrWhiteSpace(provider.AttemptedValue))
                {
                    propertyDescriptor.SetValue(bindingContext.Model, null);
                    return;
                }

                DateTime dateTime;
                if (DateTime.TryParse(provider.AttemptedValue, CultureInfo, DateTimeStyles.None, out dateTime))
                {
                    propertyDescriptor.SetValue(bindingContext.Model, dateTime);
                    return;
                }
            }

            bindingContext.ModelState.AddModelError(bindingContext.ModelName, "Data inválida");
            propertyDescriptor.SetValue(bindingContext.Model, Activator.CreateInstance(propertyType));
        }
    }
}