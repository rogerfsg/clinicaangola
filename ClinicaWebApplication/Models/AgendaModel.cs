﻿using ClinicaWebApplication.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClinicaWebApplication.Models
{
    public class AgendaModel
    {
        /// <summary>
        /// Id do serviço
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Identificador do usuário
        /// </summary>
        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual ApplicationUser User{ get; set; }

        /// <summary>
        /// Serviço referente a consulta
        /// </summary>
        [ForeignKey("Servico")]
        public int ServicoId { get; set; }
        public virtual ServicoModel Servico { get; set; }

        /// <summary>
        /// Data do inicio da consulta
        /// </summary>
        [DataType(DataType.DateTime)]
        public DateTime? DataInicioConsulta { get; set; }

        /// <summary>
        /// Data fim da consulta
        /// </summary>
        [DataType(DataType.DateTime)]
        public DateTime? DataFimConsulta { get; set; }

      

        public string FormatarDataInicioParaExibicao()
        {
            if (DataInicioConsulta == null) return null;
            else return FactoryContext.GetSingleton().HelperDate.FormatarDataEHora(DataInicioConsulta.Value);
        }

        public string FormatarDataFimParaExibicao()
        {
            if (DataFimConsulta == null) return null;
            else return FactoryContext.GetSingleton().HelperDate.FormatarDataEHora(DataFimConsulta.Value);
        }
    }
    public enum StateSlot
    {
        /// <summary>
        /// Caso a data relativa ao slot seja pasado, maior que a atual
        /// </summary>
        EXPIRADO,
        /// <summary>
        /// Caso o Slot esteja livre para agendamento
        /// </summary>
        LIVRE,
        /// <summary>
        /// Já existe agendamento de outro usuario
        /// </summary>
        OCUPADO,
        /// <summary>
        /// Ja existe um agendamento meu
        /// </summary>
        OCUPADO_PELO_USUARIO_LOGADO
    }
    public class SlotModel
    {
        /// <summary>
        /// Titulo exibido no slot do dia da semana na agenda
        /// </summary>
        public string Titulo { get; set; }
        /// <summary>
        /// Caso já exista um agendamento para o slot em questão
        /// </summary>
        public AgendaModel Agendamento { get; set; }
        /// <summary>
        /// Data de inicio do slot
        /// </summary>
        public DateTime DataInicio { get; set; }
        /// <summary>
        /// Data de fim do slot
        /// </summary>
        public DateTime DataFim { get; set; }

        private StateSlot? __StateSlot = null;
        public StateSlot StateSlot
        {
            get
            {
        //        if (__StateSlot != null) return __StateSlot.Value;
                
                DateTime now = FactoryContext.GetSingleton().HelperDate.Now();
                if ( now > DataInicio)
                    __StateSlot =StateSlot.EXPIRADO;
                else
                    __StateSlot = Agendamento != null 
                        ? (OcupadoPeloUsuarioLogado) ? StateSlot.OCUPADO_PELO_USUARIO_LOGADO  : StateSlot.OCUPADO 
                        : StateSlot.LIVRE;
                
                return __StateSlot.Value;
            }
        }

        public bool OcupadoPeloUsuarioLogado { get; set; }

        public string Descricao ()
        {
            StateSlot state = StateSlot;
            switch(state)
            {
                case ClinicaWebApplication.Models.StateSlot.LIVRE:
                    return "Agendar";
                case ClinicaWebApplication.Models.StateSlot.OCUPADO_PELO_USUARIO_LOGADO:
                    return "Ocupado por mim";
                case ClinicaWebApplication.Models.StateSlot.OCUPADO:
                    return "Ocupado";
                case ClinicaWebApplication.Models.StateSlot.EXPIRADO:
                    return "Expirado";
            }
            return "N/A";
        }

        public string CorDescritiva()
        {
            StateSlot state = StateSlot;
            switch (state)
            {
                case ClinicaWebApplication.Models.StateSlot.LIVRE:
                    return "LightGreen";
                case ClinicaWebApplication.Models.StateSlot.OCUPADO_PELO_USUARIO_LOGADO:
                    return "LightOrange";
                case ClinicaWebApplication.Models.StateSlot.OCUPADO:
                    return "LightRed";
                case ClinicaWebApplication.Models.StateSlot.EXPIRADO:
                    return "LightGray";
            }
            return "N/A";
        }


        
    }
    /// <summary>
    /// Model do dia da semana
    /// </summary>
    public class DayModel
    {
        
        /// <summary>
        /// Número do dia do mes
        /// </summary>
        public int Dia { get; set; }
        /// <summary>
        /// Slots do dia disponiveis para agendamento
        /// </summary>
        public SlotModel[] Slots { get; set; }
    }

    public enum StateCalendario
    {
        ESCOLHENDO_O_SERVICO,
        VISUALIZANDO_CALENDARIO,
        SELECIONANDO_HORARIOS,
    }

    /// <summary>
    /// Model do calendário dos dias da semana
    /// </summary>
    public class CalendarioModel
    {
        /// <summary>
        /// Titulo aplicado ao calendário da semana
        /// </summary>
        public string TituloMes { get; set; }
        /// <summary>
        /// Titulos do dia da semana: seg, ter, qua, ...
        /// </summary>
        public string[] TituloDiasSemana { get; set; }
        /// <summary>
        /// Modelos dos dias da semana
        /// </summary>
        public DayModel[] DiasDaSemana { get; set; }

        /// <summary>
        /// Email do usuário logado
        /// </summary>
        public string UsuarioEmail { get; set; }

        /// <summary>
        /// Id do serviço selecionado
        /// </summary>
        public int? SelecionadoServicoId { get; set; }
        
        public SelectList Servicos { get; set; }

        
        /// <summary>
        /// Para consultas particionadas, se mante o indicador de quantas ja foram agendadas
        /// </summary>
        public int? ContadorAgendamento { get;  set; }

        /// <summary>
        /// Total de consultas permitidas para agendamento de acordo com o servico selecionado
        /// </summary>
        public ServicoModel SelecionadoServico { get; set; }

        public int GetTotalRestanteParaAgendamento()
        {
            if (ContadorAgendamento != null && SelecionadoServico != null)
            {
                return SelecionadoServico.Intervalo - ContadorAgendamento.Value;
            }
            else
                return 0;
        }
        /// <summary>
        /// Alguma data da semana que está sendo exibida no calendario
        /// </summary>
        public DateTime? DataDaSemana { get; set; }

        public StateCalendario StateCalendario
        {
            get
            {
                int tot = GetTotalRestanteParaAgendamento();
                if (tot > 0) return StateCalendario.SELECIONANDO_HORARIOS;
                else if (SelecionadoServicoId == null) return StateCalendario.ESCOLHENDO_O_SERVICO;
                else return StateCalendario.VISUALIZANDO_CALENDARIO;
            }
        }

        /// <summary>
        /// Mensagem a ser exibida ao usuário de acordo com a ultima acao
        /// </summary>
        public string MensagemUsuario { get; set; }

        /// <summary>
        /// Checa se esse slot é passivel de cancelamento
        /// </summary>
        /// <param name="slotModel"></param>
        /// <returns></returns>
        public bool CancelamentoPermitido(SlotModel slotModel)
        {
            if (slotModel.StateSlot != StateSlot.OCUPADO_PELO_USUARIO_LOGADO)
                return false;

            //apenas StateSlot.OCUPADO_PELO_USUARIO_LOGADO podem seguir
            DateTime now = FactoryContext.GetSingleton().HelperDate.Now();
            switch (SelecionadoServico.TipoParaCancelamento)
            {
                //Se for dia corrido acrescenta a UnidadeMaximaParaCancelamento como dias a data atual para verificar se ultrapassou 
                //o limite permitido para o cancelamento
                case ServicoModel.TipoCancelamento.DIA_CORRIDO:
                    return slotModel.DataInicio.Date.AddDays(-SelecionadoServico.UnidadeMaximaParaCancelamento) > now.Date;
                //idem porem com grandeza de hora
                case ServicoModel.TipoCancelamento.HORA_CORRIDA:
                    return slotModel.DataInicio.AddHours(-SelecionadoServico.UnidadeMaximaParaCancelamento) > now;
            }
            return false;
        }
    }

}