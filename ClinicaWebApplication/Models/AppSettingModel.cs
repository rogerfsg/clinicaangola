﻿using ClinicaWebApplication.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ClinicaWebApplication.Models
{
    public class AppSettingModel
    {


        /// <summary>
        /// <!-- Formato deve ser HH24:MM -->
        /// <add key = "HorarioInicioMarcacao" value="08:00" />
        /// </summary>
        public TimeSpan HorarioInicioMarcacao
        {
            get
            {

                return FactoryContext.GetSingleton().HelperAppSettings.GetTimeSpanDoAppSettings("HorarioInicioMarcacao");
            }
        }

        public TimeSpan HorarioFimMarcacao
        {
            get
            {
                return FactoryContext.GetSingleton().HelperAppSettings.GetTimeSpanDoAppSettings("HorarioInicioMarcacao");
            }
        }
    }
}