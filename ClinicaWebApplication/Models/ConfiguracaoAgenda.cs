﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ClinicaWebApplication.Model
{
    public class ConfiguracaoAgenda
    {   
        public TimeSpan TsInicio { get; set; }
        public TimeSpan TsFim {get;set;}
        public DateTime Now { get; set; }
        public DateTime StartWeek { get; set; }
        public DateTime EndWeek { get; set; }
        
        public string[] Days { get; set; }

        public CultureInfo CultureInfo { get; set; }
    }
}
