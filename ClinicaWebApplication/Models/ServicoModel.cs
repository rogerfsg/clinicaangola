﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ClinicaWebApplication.Models
{
    public class ServicoModel
    {
        /// <summary>
        /// Id do serviço
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Nome do serviço
        /// </summary>
        [StringLength(50)]
        public string Nome { get; set; }

        /// <summary>
        /// Duranção maxima da consulta do serviço
        /// </summary>
        public int DuracaoMinutos { get; set; }

        /// <summary>
        /// Intervalo máximo permitido para divisão da duração. Ex.: 
        ///     DuracaoMinutos: 60
        ///     Intevalo: 2
        ///     Então:
        ///         1 consulta de 60 min
        ///         2 consultas de 30 min
        /// </summary>
        public int Intervalo { get; set; }

        /// <summary>
        /// Tipo de cancelamento permitido pelo serviço
        /// </summary>
        public TipoCancelamento? TipoParaCancelamento { get; set; }

        /// <summary>
        /// Quantidade da unidade selecionada para cancelamento, se:
        ///     DIA_CORRIDO: 1 representa que o cancelamento da segunda pode ocorrer no maximo no domingo
        ///     HORA_CORRIDA: 1 repreta que o cancelamento as 13:00 da segunda feira pode ocorrer até 12:00 da segunda feira
        /// </summary>
        public int UnidadeMaximaParaCancelamento { get; set; }

        /// <summary>
        /// Tipos de cancelamento de agendamento do serviço
        /// </summary>
        public enum TipoCancelamento
        {
            /// <summary>
            /// O cancelamento só pode ocorrer de um dia para o outro, ou seja, não se pode cancelar um encontro na segunda
            /// </summary>
            [Display(Name="Dia corrido")]
            DIA_CORRIDO,
            /// <summary>
            /// O cancelamento pode ocorrer em até X horas antes do horário
            /// </summary>
            [Display(Name = "Hora corrida")]
            HORA_CORRIDA
        }


    }
}