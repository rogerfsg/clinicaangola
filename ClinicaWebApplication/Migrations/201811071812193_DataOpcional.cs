namespace ClinicaWebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataOpcional : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AgendaModels", "DataInicioConsulta", c => c.DateTime());
            AlterColumn("dbo.AgendaModels", "DataFimConsulta", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AgendaModels", "DataFimConsulta", c => c.DateTime(nullable: false));
            AlterColumn("dbo.AgendaModels", "DataInicioConsulta", c => c.DateTime(nullable: false));
        }
    }
}
