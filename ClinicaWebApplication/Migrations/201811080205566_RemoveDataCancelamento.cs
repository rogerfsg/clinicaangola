namespace ClinicaWebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveDataCancelamento : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AgendaModels", "DataCancelamentoConsulta");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AgendaModels", "DataCancelamentoConsulta", c => c.DateTime());
        }
    }
}
