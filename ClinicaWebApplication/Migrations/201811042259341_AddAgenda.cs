namespace ClinicaWebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAgenda : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ServicoModels", "AspNetUsersID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ServicoModels", "AspNetUsersID", c => c.Int(nullable: false));
        }
    }
}
