namespace ClinicaWebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateUsers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AgendaModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        ServicoId = c.Int(nullable: false),
                        DataInicioConsulta = c.DateTime(nullable: false),
                        DataFimConsulta = c.DateTime(nullable: false),
                        DataCancelamentoConsulta = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ServicoModels", t => t.ServicoId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ServicoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AgendaModels", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AgendaModels", "ServicoId", "dbo.ServicoModels");
            DropIndex("dbo.AgendaModels", new[] { "ServicoId" });
            DropIndex("dbo.AgendaModels", new[] { "UserId" });
            DropTable("dbo.AgendaModels");
        }
    }
}
