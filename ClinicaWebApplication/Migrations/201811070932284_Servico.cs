namespace ClinicaWebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Servico : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ServicoModels", "Nome", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ServicoModels", "Nome");
        }
    }
}
