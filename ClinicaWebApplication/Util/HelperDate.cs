﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicaWebApplication.Util
{
    public class HelperDate: IHelperDate
    {
       
        public DateTime Now()
        {
            return DateTime.Now;
        }

        public DateTime PrimeiroDiaDoMes(DateTime date)
        {
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            return firstDayOfMonth;
        }
        
        public DateTime UltimoDiaDoMes(DateTime date)
        {
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            return lastDayOfMonth;
        }

        public DateTime GetDiaDaSemanaRelativo(DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }

        public string FormatarHorario(DateTime dt)
        {
            return dt.ToString("HH:mm"); 
        }

        public string FormatarDataEHora(DateTime dt)
        {
            return dt.ToString("dd/MM/yyyy HH:mm");
        }
    }
}
