﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ClinicaWebApplication.Util
{
    public class HelperAppSettings : IHelperAppSettings
    {
        public string GetString(string constante)
        {
            string token = ConfigurationManager.AppSettings[constante];
            return token;
        }

        public TimeSpan GetTimeSpanDoAppSettings(string constante)
        {
            string token = ConfigurationManager.AppSettings[constante];
            return this.GetTimeSpanFromString(token);
        }

        private TimeSpan GetTimeSpanFromString(string token)
        {
            int hora = -1;
            int minuto = -1;
            if (String.IsNullOrEmpty(token)
                || token.Length != 5
                || !token.Substring(2, 1).Equals(":")
                || !Int32.TryParse(token.Substring(0, 2), out hora)
                || !Int32.TryParse(token.Substring(3, 2), out minuto)
                )
                throw new UsuarioException(
                    string.Format(
                        "Formato inválido do horario contido no web.config: {0}. " +
                        "Formato deveria ser HH24:mm. Ex.: 16:00 "
                        , token));
            return new TimeSpan(hora, minuto, 0);
        }
    }


}
