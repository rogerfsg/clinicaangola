﻿using ClinicaWebApplication.Model;
using ClinicaWebApplication.Models;
using ClinicaWebApplication.Services;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ClinicaWebApplication.Util
{
    public class FactoryProviderMock : IFactoryProvider
    {
        AgendaMock agendaMock;
        UsuarioMock usuarioMock;
        ServicoMock servicoMock;
        public FactoryProviderMock(
            
            AgendaMock agendaMock = null, 
            UsuarioMock usuarioMock = null,
            ServicoMock servicoMock = null)
        {
            this.agendaMock = agendaMock==null ? new AgendaMock() : null;
            this.usuarioMock = usuarioMock  ==null ? new UsuarioMock() : null;
            this.servicoMock = servicoMock ==null?new ServicoMock() : null;
            
            //var context = new Mock<HttpContextBase>();
            //var mockIdentity = new Mock<IIdentity>();
            //Substitute.For()
            //context.SetupGet(x => x.User.Identity).Returns(mockIdentity.Object);
            //mockIdentity.Setup(x => x.Name).Returns("test_name");
        }

        public override void Dispose()
        {
         
        }
        public class Provider : IProvider
        {
            object fakeServer = new object();
            public void Dispose()
            { }

            public object getProvider()
            { return fakeServer; }
        }

        public override IAgendaDataProvider FactoryAgendaDataProvider(IProvider provider, ConfiguracaoAgenda configuracaoAgenda)
        {
            return agendaMock;
        }

        public override IProvider FactoryProvider()
        {
            return new Provider();
        }

        public override IUsuarioDataProvider FactoryUsuarioDataProvider(IProvider provider, string userId)
        {
            return usuarioMock;
        }

        public override IServicoDataProvider FactoryServicoDataProvider(IProvider provider)
        {
            return servicoMock;
        }


    }
}
