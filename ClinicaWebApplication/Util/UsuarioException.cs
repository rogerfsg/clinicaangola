﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicaWebApplication.Util
{
    public class UsuarioException : Exception
    {
        public string ResourceReferenceProperty { get; set; }

        public UsuarioException()
        {
        }

        public UsuarioException(string message)
            : base(message)
        {
        }

        public UsuarioException(string message, Exception inner)
            : base(message, inner)
        {
        }

    }
}
