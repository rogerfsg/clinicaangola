﻿using ClinicaWebApplication.Model;
using ClinicaWebApplication.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ClinicaWebApplication.Util
{
    
    /// <summary>
    /// Factory method pattern
    /// </summary>
    public interface IFactoryAgenda
    {
      
        ConfiguracaoAgenda factoryConfiguracao();

        

        DayModel[] factoryDays(List<AgendaModel> horariosMarcados);
    }

    
}
