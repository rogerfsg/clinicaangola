﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicaWebApplication.Util
{
    public interface IHelperAppSettings
    {
        /// <summary>
        /// Retorna o timestamp do horario contido na constante do repositorio
        /// </summary>
        /// <param name="constante">Nome da constante contida no repositorio de configuracao</param>
        /// <returns></returns>
        TimeSpan GetTimeSpanDoAppSettings(string constante);

        /// <summary>
        /// Retorna uma string armazenada na constante
        /// </summary>
        /// <param name="constante"></param>
        /// <returns></returns>
        string GetString(string constante);
    }
}
