﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicaWebApplication.Util
{
    public interface IHelperDate
    {
        /// <summary>
        /// Retorna o horario atual
        /// </summary>
        /// <returns></returns>
        DateTime Now();

        /// <summary>
        /// Retorna  a data apontando para o primeiro dia do mes
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        DateTime PrimeiroDiaDoMes(DateTime date);

        /// <summary>
        /// Retorna a data apontando para o ultimo dia do mes
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        DateTime UltimoDiaDoMes(DateTime date);

        /// <summary>
        /// Retorna o objeto Date apontando para o dia da semana relativo a data de entrada
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="startOfWeek"></param>
        /// <returns></returns>
        DateTime GetDiaDaSemanaRelativo(DateTime dt, DayOfWeek startOfWeek);


        /// <summary>
        /// Retorna o horário formatado para exibição
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        string FormatarHorario(DateTime dt);

        /// <summary>
        /// Retorna a data e a hora DD/MM/YYYY HH24:mm 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        string FormatarDataEHora(DateTime dt);

    }
}
