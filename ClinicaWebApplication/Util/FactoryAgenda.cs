﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using ClinicaWebApplication.Model;
using ClinicaWebApplication.Models;

namespace ClinicaWebApplication.Util
{
    public class FactoryAgenda : IFactoryAgenda
    {
        ConfiguracaoAgenda config = null;
        string idUsuario;
        DateTime? dataDaSemana = null;
        public FactoryAgenda(string idUsuario, DateTime? dataDaSemana)
        {
            this.idUsuario = idUsuario;
            this.dataDaSemana = dataDaSemana;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataDaSemana">Alguma data que esteja na semana de interesse</param>
        /// <returns></returns>
        public ConfiguracaoAgenda factoryConfiguracao()
        {
            if (config != null) return config;
            ConfiguracaoAgenda conf = new ConfiguracaoAgenda();
            try
            {
                FactoryContext factory = FactoryContext.GetSingleton();

                IHelperAppSettings helperAS = factory.HelperAppSettings;
                IHelperDate helperD = factory.HelperDate;
                
                conf.TsInicio = helperAS.GetTimeSpanDoAppSettings("HorarioInicioAgendamento");
                conf.TsFim = helperAS.GetTimeSpanDoAppSettings("HorarioFimAgendamento");
                conf.Now = helperD.Now();
                if (dataDaSemana == null) dataDaSemana = conf.Now;
                conf.StartWeek = helperD.GetDiaDaSemanaRelativo(dataDaSemana.Value, DayOfWeek.Sunday);
                conf.StartWeek = conf.StartWeek.Date;
                conf.StartWeek = conf.StartWeek.Add(conf.TsInicio);
                conf.EndWeek = dataDaSemana.Value.DayOfWeek == DayOfWeek.Saturday
                    ? dataDaSemana.Value
                    : helperD.GetDiaDaSemanaRelativo(dataDaSemana.Value.AddDays(7), DayOfWeek.Saturday);
                //Avanca ate o ultimo segundo do dia
                conf.EndWeek = conf.EndWeek.Date.AddDays(1).AddTicks(-1);
                CultureInfo cultureInfo = factory.CultureInfo;
                conf.Days = cultureInfo.DateTimeFormat.DayNames;
                conf.CultureInfo = cultureInfo;
            }
            finally
            {
                config = conf;
            }
            
            return conf;
        }
       

        public DayModel[] factoryDays(List<AgendaModel> horariosMarcados)
        {
            FactoryContext factoryContext = FactoryContext.GetSingleton();
            IFactorySlots factorySlots= factoryContext.FactorySlots;
            ConfiguracaoAgenda conf = factoryConfiguracao();
            DayModel[] dias = new DayModel[conf.Days.Length];
            

            for (int i = 0; i < conf.Days.Length; i++, conf.StartWeek = conf.StartWeek.AddDays(1))
            {
                FactorySlots.BuilderSlot builder = new FactorySlots.BuilderSlot();
                builder.data = conf.StartWeek;
                builder.intervalo= new TimeSpan(0, 30, 0);
                builder.horaInicio= conf.TsInicio;
                builder.horaFim = conf.TsFim;
                builder.horariosMarcados=horariosMarcados;
                builder.idUser= this.idUsuario;
                
                dias[i] = new DayModel
                {
                    Dia = conf.StartWeek.Day,
                    Slots = factorySlots.GetSlotsDoDia(builder)
                };
            }
            return dias;
        }

    }
}
