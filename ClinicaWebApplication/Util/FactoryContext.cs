﻿using ClinicaWebApplication.Models;
using ClinicaWebApplication.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ClinicaWebApplication.Util
{
   

    public class FactoryContext 
    {
        private IFactoryProvider __factoryProvider;
        public IFactoryProvider FactoryDataProvider
        {
            get {
                if(__factoryProvider == null) 
                    __factoryProvider = IFactoryProvider.factory(HelperAppSettings) ;
                return __factoryProvider; 
            }
            set {
                __factoryProvider = value == null ? IFactoryProvider.factory(HelperAppSettings) : value;
            }
        }

        private FactoryContext() {

            
        }
        static FactoryContext Singleton=null;
        public static FactoryContext GetSingleton()
        {
            if (Singleton == null)
                Singleton = new FactoryContext();
            return Singleton;
        }
       

        private IHelperAppSettings __helperAppSettings;
        public IHelperAppSettings HelperAppSettings {
            get
            {
                if(__helperAppSettings ==null)
                    __helperAppSettings = new HelperAppSettings();
                return __helperAppSettings;
            }
        }

        private IHelperDate __helperDate;
        public IHelperDate HelperDate
        {
            get
            {
                if (__helperDate== null)
                    __helperDate = new HelperDate();
                return __helperDate;
            }
        }

        public CultureInfo CultureInfo
        {
            get
            {
                return CultureInfo.CurrentCulture;
            }
        }

        public IFactorySlots FactorySlots
        {
            get
            {
                return new FactorySlots();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <param name="dataDaSemana">Alguma data do calendario da semana visivel. Null se quiser que seja a data corrente.</param>
        /// <returns></returns>
        public IFactoryAgenda FactoryAgenda(string idUsuario, DateTime? dataDaSemana)
        {

            return new FactoryAgenda(idUsuario, dataDaSemana);

        }
        
        public IProvider FactoryProvider()
        {
            return FactoryDataProvider.FactoryProvider();
        }

        public IUsuarioDataProvider FactoryUsuarioDataProvider(IProvider provider, string userId)
        {
            return FactoryDataProvider.FactoryUsuarioDataProvider(provider, userId);
        }

        public IServicoDataProvider FactoryServicoDataProvider(IProvider provider)
        {
            return FactoryDataProvider.FactoryServicoDataProvider(provider); 
        }


        public IAgendaDataProvider FactoryAgendaDataProvider(IProvider provider, IFactoryAgenda factoryAgenda)
        {
            return FactoryDataProvider.FactoryAgendaDataProvider(provider, factoryAgenda.factoryConfiguracao());
            
        }

    }
}
