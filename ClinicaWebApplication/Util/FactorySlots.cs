﻿using ClinicaWebApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicaWebApplication.Util
{
    public class FactorySlots : IFactorySlots
    {
        public class BuilderSlot
        {
            public DateTime data;
            public TimeSpan intervalo;
            public TimeSpan horaInicio;
            public TimeSpan horaFim;
            public List<AgendaModel> horariosMarcados;
            public string idUser;
        }

        const string FORMATO_HORA = "HH:mm";
        public SlotModel[] GetSlotsDoDia(BuilderSlot parametros )
        {
            
            List<SlotModel> slots = new List<SlotModel>();

            DateTime dataInicio = parametros. data.Date.AddHours(parametros.horaInicio.Hours);
            dataInicio = dataInicio.AddMinutes(parametros.horaInicio.Minutes);

            DateTime ultimaData = parametros.data.Date.AddHours(parametros.horaFim.Hours);
            ultimaData = ultimaData.AddMinutes(parametros.horaFim.Minutes);
            
            DateTime now = FactoryContext.GetSingleton().HelperDate.Now();

            while (dataInicio < ultimaData)
            {
                DateTime dataFim = dataInicio.Add(parametros.intervalo);
                SlotModel s = new SlotModel
                {
                    Titulo = String.Format(
                        "{0} até {1}",
                        dataInicio.ToString(FORMATO_HORA) ,
                        dataFim.ToString(FORMATO_HORA)),
                    DataInicio = dataInicio,
                    DataFim = dataFim,
                    
                };
                if(parametros.horariosMarcados.Count > 0)
                {
                    AgendaModel agenda = parametros.horariosMarcados[0];
                    
                    //Se o horario no topo da lista ordenada ja for do passado
                    //ele será removido
                    while (parametros.horariosMarcados.Count > 0 
                            && agenda.DataFimConsulta <= s.DataInicio)
                    {
                        parametros.horariosMarcados.RemoveAt(0);
                        agenda = parametros.horariosMarcados.Count > 0 ? parametros.horariosMarcados[0] : null;
                    }
                    
                    if (//Existe agendamento
                        agenda != null
                        //Verifica se o intervalo do slot coincide com o intervalo da proxima consulta marcada cronologicamente
                        && s.DataInicio >= agenda.DataInicioConsulta 
                        && (s.DataInicio < agenda.DataFimConsulta  || s.DataFim < agenda.DataFimConsulta))
                    {
                        s.Agendamento = agenda;
                        s.OcupadoPeloUsuarioLogado = agenda.UserId == parametros.idUser;
                    }
                }

                slots.Add(s);
                dataInicio = dataFim;
            }
            return slots.ToArray();
        }

        
    }
}