﻿using ClinicaWebApplication.Model;
using ClinicaWebApplication.Models;
using ClinicaWebApplication.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ClinicaWebApplication.Util
{
    public class FactoryProviderBancoDeDados : IFactoryProvider
    {
        
        public FactoryProviderBancoDeDados() {
            
        }
        
        
        public override IAgendaDataProvider FactoryAgendaDataProvider(IProvider provider, ConfiguracaoAgenda configuracaoAgenda)
        {   
            return new AgendaBancoDeDados(provider, configuracaoAgenda);
        }

        public override IProvider FactoryProvider()
        {   
            return new ProviderBancoDeDados();
        }

        public override IUsuarioDataProvider FactoryUsuarioDataProvider(IProvider provider, string userId)
        {
            return new UsuarioBancoDeDados(provider, userId);
        }

        public override IServicoDataProvider FactoryServicoDataProvider(IProvider provider)
        {
            return new ServicoBancoDeDados(provider);
        }

        public override void Dispose()
        {
            
        }
    }
}
