﻿using ClinicaWebApplication.Model;
using ClinicaWebApplication.Models;
using ClinicaWebApplication.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ClinicaWebApplication.Util
{
    public abstract class IFactoryProvider : IDisposable
    {
        //public enum ConfiguracoesProvider
        //{
        //    BANCO,
        //    MOCK
        //}

        //const string CONFIGURACAO_PROVIDER = "ConfiguracaoProvider";
        
        public static IFactoryProvider factory(IHelperAppSettings appSettings)
        {
            //string settings= appSettings.GetString(CONFIGURACAO_PROVIDER);
            //settings=settings != null ? settings.ToUpper() : null;
            //if (settings != null && ConfiguracoesProvider.BANCO.ToString().Equals(settings))
                
                return new FactoryProviderBancoDeDados();
            //else return new FactoryProviderMock();
            
        }

        public abstract IProvider FactoryProvider();

        public abstract IUsuarioDataProvider FactoryUsuarioDataProvider(IProvider provider, string userId);

        public abstract IServicoDataProvider FactoryServicoDataProvider(IProvider provider);

        public abstract IAgendaDataProvider FactoryAgendaDataProvider(IProvider provider, ConfiguracaoAgenda configuracaoAgenda);

        public abstract void Dispose();
    }
}
