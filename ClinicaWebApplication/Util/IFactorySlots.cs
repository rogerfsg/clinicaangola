﻿using ClinicaWebApplication.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using static ClinicaWebApplication.Util.FactorySlots;

namespace ClinicaWebApplication.Util
{
    public interface IFactorySlots
    {
        /// <summary>
        /// Retorna os slots disponiveis para agendamento da semana
        /// </summary>
        /// <param name="builderSlot">Builder contendo os campos necessarios para construcao dos slots</param>
        /// <returns></returns>
        SlotModel[] GetSlotsDoDia(BuilderSlot builderSlot);
    }
}
